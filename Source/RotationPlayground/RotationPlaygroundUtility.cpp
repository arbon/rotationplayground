#include "RotationPlaygroundUtility.h"

FRotator URotationPlaygroundUtility::ParseCommandString(const TArray<TScriptInterface<IRotationCommand>>& Commands, FString CommandString, bool& Succeeded, FString& ErrorString)
{
    struct Local
    {
        static TScriptInterface<IRotationCommand> FindCommand(FName CommandName, const TArray<TScriptInterface<IRotationCommand>>& Commands)
        {
            for (auto Command : Commands)
            {
                if (Command->Execute_GetName(Command.GetObject()).IsEqual(CommandName)) return Command;
            }

            return nullptr;
        }

        static float ProcessFloatLiteral(FString FloatString)
        {
            return FCString::Atof(*FloatString);
        }
    };

    TArray<FString> CommandStrings;

    CommandString = CommandString.TrimStartAndEnd();

    FRotator CurrentResult = FRotator(0.0f, 0.0f, 0.0f);
    TArray<TScriptInterface<IRotationCommand>> CommandStack;
    TArray<int> CommandStackParamIndices;
    TArray<FRotationCommandContext> CommandStackContexts;

    // Extract Literals
    int CursorPosition = 0;
    while (CursorPosition < CommandString.Len())
    {
        int EndCommandIndex = -1;
        if (!CommandString.FindChar(TCHAR(' '), EndCommandIndex))
        {
            EndCommandIndex = CommandString.Len();
        }

        FString CurrentCommandString = CommandString.Left(EndCommandIndex);
        FName CurrentCommandName = FName(*CurrentCommandString);
        CommandString = CommandString.RightChop(EndCommandIndex + 1);
        TScriptInterface<IRotationCommand> CurrentCommand = Local::FindCommand(CurrentCommandName, Commands);

        if (CurrentCommand)
        {
            CommandStack.Add(CurrentCommand);
            CommandStackParamIndices.Add(0);
            CommandStackContexts.Add(FRotationCommandContext());
        }
        else {
            float LiteralFloat = Local::ProcessFloatLiteral(CurrentCommandString);
            auto PeekCommand = CommandStack.Last();
            int& PeekIndex = CommandStackParamIndices.Last();
            FRotationCommandContext& PeekContext = CommandStackContexts.Last();
            auto PeekParamTypes = PeekCommand->Execute_GetParamTypes(PeekCommand.GetObject());
            if (PeekParamTypes.Num() > PeekIndex && PeekParamTypes[PeekIndex] == ERotationCommandType::Float)
            {
                PeekContext.FloatParams.Add(LiteralFloat);
                PeekIndex++;
            }
            else
            {
                Succeeded = false;
                ErrorString = FString::Printf(TEXT("Unexpected Float Literal at %d"), CursorPosition);
                return FRotator();
            }
        }

        while (CommandStack.Last()->GetParamTypes().Num() == CommandStackParamIndices.Last() - 1)
        {
            auto PopCommandStack = CommandStack.Pop();
            auto PopCommandStackContext = CommandStackContexts.Pop();
            int PopCommandStackParamIndices = CommandStackParamIndices.Pop();

            switch (PopCommandStack->GetReturnType())
            {
                case ERotationCommandType::Float:
                {
                    float FloatResult = PopCommandStack->Execute_GetFloat(PopCommandStack.GetObject(), PopCommandStackContext);

                    if (CommandStack.Num() == 0) {
                        Succeeded = false;
                        ErrorString = FString::Printf(TEXT("Final result is float (%d), should be rotator"), FloatResult);
                        return FRotator();
                    }

                    auto PeekCommand = CommandStack.Last();
                    int& PeekIndex = CommandStackParamIndices.Last();
                    FRotationCommandContext& PeekContext = CommandStackContexts.Last();
                    auto PeekParamTypes = PeekCommand->Execute_GetParamTypes(PeekCommand.GetObject());
                    if (PeekParamTypes.Num() > PeekIndex && PeekParamTypes[PeekIndex] == ERotationCommandType::Float)
                    {
                        PeekContext.FloatParams.Add(FloatResult);
                        PeekIndex++;
                    }
                    else
                    {
                        Succeeded = false;
                        ErrorString = FString::Printf(TEXT("Unexpected Float Literal at %d"), CursorPosition);
                        return FRotator();
                    }

                    break;
                }
                case ERotationCommandType::Rotator:
                {
                    FRotator RotatorResult = PopCommandStack->Execute_GetRotator(PopCommandStack.GetObject(), PopCommandStackContext);

                    if (CommandStack.Num() == 0) {
                        Succeeded = true;
                        return RotatorResult;
                    }

                    auto PeekCommand = CommandStack.Last();
                    int& PeekIndex = CommandStackParamIndices.Last();
                    FRotationCommandContext& PeekContext = CommandStackContexts.Last();
                    auto PeekParamTypes = PeekCommand->Execute_GetParamTypes(PeekCommand.GetObject());
                    if (PeekParamTypes.Num() > PeekIndex && PeekParamTypes[PeekIndex] == ERotationCommandType::Rotator)
                    {
                        PeekContext.RotatorParams.Add(RotatorResult);
                        PeekIndex++;
                    }
                    else
                    {
                        Succeeded = false;
                        ErrorString = FString::Printf(TEXT("Unexpected Rotator at %d"), CursorPosition);
                        return FRotator();
                    }

                    break;
                }
                case ERotationCommandType::Vector:
                {
                    FVector VectorResult = PopCommandStack->Execute_GetVector(PopCommandStack.GetObject(), PopCommandStackContext);

                    if (CommandStack.Num() == 0) {
                        Succeeded = false;
                        ErrorString = FString::Printf(TEXT("Final result is vector (%s), should be rotator"), *VectorResult.ToCompactString());
                        return FRotator();
                    }

                    auto PeekCommand = CommandStack.Last();
                    int& PeekIndex = CommandStackParamIndices.Last();
                    FRotationCommandContext& PeekContext = CommandStackContexts.Last();
                    auto PeekParamTypes = PeekCommand->Execute_GetParamTypes(PeekCommand.GetObject());
                    if (PeekParamTypes.Num() > PeekIndex && PeekParamTypes[PeekIndex] == ERotationCommandType::Vector)
                    {
                        PeekContext.VectorParams.Add(VectorResult);
                        PeekIndex++;
                    }
                    else
                    {
                        Succeeded = false;
                        ErrorString = FString::Printf(TEXT("Unexpected Vector at %d"), CursorPosition);
                        return FRotator();
                    }

                    break;
                }
                default:
                    break;
            }
        }

        CursorPosition += EndCommandIndex + 1;
    }

    Succeeded = false;
    ErrorString = FString::Printf(TEXT("Unexpected Float Literal at %d"), CursorPosition);
    return FRotator();
}
