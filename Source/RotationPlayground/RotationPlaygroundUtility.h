// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "RotationCommand.h"
#include "RotationPlaygroundUtility.generated.h"

/**
 * 
 */
UCLASS()
class ROTATIONPLAYGROUND_API URotationPlaygroundUtility : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

    UFUNCTION(BlueprintCallable)
    static FRotator ParseCommandString(const TArray<TScriptInterface<IRotationCommand>>& Commands, FString CommandString, bool& Succeeded, FString& ErrorString);
};
