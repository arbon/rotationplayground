// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "RotationCommand.generated.h"

UENUM(BlueprintType)
enum class ERotationCommandType : uint8
{
    Rotator,
    Float,
    Vector
};

USTRUCT(BlueprintType)
struct FRotationCommandContext
{
    GENERATED_BODY()

    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    TArray<float> FloatParams;

    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    TArray<FVector> VectorParams;

    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    TArray<FRotator> RotatorParams;
};

// This class does not need to be modified.
UINTERFACE(Blueprintable)
class URotationCommand : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class ROTATIONPLAYGROUND_API IRotationCommand
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    FName GetName();

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    FString GetDescription();

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    ERotationCommandType GetReturnType();

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    TArray<ERotationCommandType> GetParamTypes();

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    FRotator GetRotator(const FRotationCommandContext& CommandContext);

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    float GetFloat(const FRotationCommandContext& CommandContext);

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    FVector GetVector(const FRotationCommandContext& CommandContext);
};
