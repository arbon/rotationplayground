// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RotationPlaygroundGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ROTATIONPLAYGROUND_API ARotationPlaygroundGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
